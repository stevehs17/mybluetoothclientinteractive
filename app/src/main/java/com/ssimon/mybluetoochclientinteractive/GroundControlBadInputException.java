package com.ssimon.mybluetoochclientinteractive;

public class GroundControlBadInputException extends Exception {
    public GroundControlBadInputException() {
        super();
    }

    public GroundControlBadInputException(Exception e) {
        super(e);
    }

    public GroundControlBadInputException(String message) {
        super(message);
    }
}
