package com.ssimon.mybluetoochclientinteractive;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class BluetoothClient {
    static final private String START_BREW_COMMAND = "7";

    private BluetoothClient() {}

    static BluetoothSocket connect(Activity act) throws GroundControlBluetoothException {
        CheckerUtils.notNull(act, "activity");

        //final String address = "98:76:B6:00:35:6B";   // for Arduino
        //final String address = "F0:4F:7C:A1:0C:81";   // for my Kindle
        //final String address = "18:83:31:F2:59:CC";  // for my Samsung Galaxy phone
        //final String address = "00:18:DB:01:0E:CB"; // for PLC controller
        final String address = "F0:5B:7B:93:B0:3D";
        final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

        BluetoothAdapter adapt = BluetoothAdapter.getDefaultAdapter();
        if (adapt == null)
            throw new IllegalStateException("Bluetooth not supported");
        if (!adapt.isEnabled())
            throw new GroundControlBluetoothException();
        BluetoothDevice dev = adapt.getRemoteDevice(address);
        try {
            BluetoothSocket sock = dev.createRfcommSocketToServiceRecord(uuid);
            adapt.cancelDiscovery();
            sock.connect();
            return sock;
        } catch (IOException e) {
            throw new GroundControlBluetoothException(e);
        }
    }

    static void write(BluetoothSocket sock, String data) throws GroundControlBluetoothException {
        CheckerUtils.notNull(sock, "socket");
        CheckerUtils.notNullOrEmpty(data, "data");

        try {
            OutputStream o = sock.getOutputStream();
            o.write(data.getBytes());
            o.flush(); // necessary?
        } catch (IOException e) {
            throw new GroundControlBluetoothException(e);
        }
    }

    static String read(BluetoothSocket sock) throws GroundControlBluetoothException {
        try {
            InputStream in = sock.getInputStream();
            byte[] buf = new byte[1024];
            int nbytes = in.read(buf);
            String s = new String(buf, 0, nbytes);
            return s;
        } catch (IOException e) {
            throw new GroundControlBluetoothException(e);
        }
    }

}
