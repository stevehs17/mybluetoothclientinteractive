package com.ssimon.mybluetoochclientinteractive;

public class GroundControlBluetoothException extends Exception {
    public GroundControlBluetoothException() {
        super();
    }

    public GroundControlBluetoothException(Exception e) {
        super(e);
    }

    public GroundControlBluetoothException(String message) {
        super(message);
    }
}
