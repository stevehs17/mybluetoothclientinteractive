package com.ssimon.mybluetoochclientinteractive;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

interface Constants {

    int MIN_VOLUME = 355;
    int VOLUME_355_ML = MIN_VOLUME;
    int VOLUME_473_ML = 473;
    int VOLUME_1000_ML = 1000;
    int VOLUME_3000_ML = 3000;
    int VOLUME_5000_ML = 5000;
    int MAX_VOLUME = VOLUME_5000_ML;

    int MIN_CYCLES = 1;
    int MAX_CYCLES = 6;

    int MIN_BREW_TIME_SECONDS = 30;
    int MAX_BREW_TIME_SECONDS = 999;


}
