package com.ssimon.mybluetoochclientinteractive;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

class Utils {
    static final private String PREFERENCES_FILE = "change_temperature_activity_preferences";
    static final private String TEMPERATURE_KEY = "temperature_key";

    private Utils() {}

    static int booleanToInt(boolean b) {
        return b ? 1 : 0;
    }

    static boolean intToBoolean(int n) {
        return n == 0 ? false : true;
    }

    static String booleanToIntString(boolean b) {
        return b? "1" : "0";
    }

    static void showToast(Context c, String msg) {
        CheckerUtils.notNull(c, "context");
        CheckerUtils.notNullOrEmpty(msg, "message");

        Toast.makeText(c, msg, Toast.LENGTH_LONG).show();
    }

    static void showToast(Context c, int stringId) {
        CheckerUtils.notNull(c, "context");
        CheckerUtils.atLeast(stringId, 0, "string id");

        Toast.makeText(c, c.getString(stringId), Toast.LENGTH_LONG).show();
    }

    static void showAlert(Context con, int stringId) {
        CheckerUtils.notNull(con, "context");
        CheckerUtils.atLeast(stringId, 0, "stringId");

        AlertDialog ad = new AlertDialog.Builder(con).create();
        ad.setMessage(con.getString(stringId));
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int unused) {
                        dialogInterface.dismiss();
                    }
                });
        ad.show();
    }

    static void showAlert(Context con, String msg) {
        CheckerUtils.notNull(con, "context");
        CheckerUtils.notNullOrEmpty(msg, "msg");

        AlertDialog ad = new AlertDialog.Builder(con).create();
        ad.setMessage(msg);
        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int unused) {
                        dialogInterface.dismiss();
                    }
                });
        ad.show();
    }

 }
