package com.ssimon.mybluetoochclientinteractive;

import android.bluetooth.BluetoothSocket;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    static final private String TAG = "MainActivity";
    private BluetoothSocket mBluetoothSocket = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        keepScreenOn();
    }

    private void keepScreenOn() {
        View v = findViewById(android.R.id.content);
        v.setKeepScreenOn(true);
    }

    public void onClickConnect(View unused) {
        try {
            mBluetoothSocket = BluetoothClient.connect(this);
            Log.v(TAG, "Connected");
        } catch (GroundControlBluetoothException e) {
            Utils.showAlert(this, e.getMessage());
        }
    }

    public void onClickWrite(View unused) {
        if (mBluetoothSocket == null) {
            Utils.showAlert(this, "socket is null");
            return;
        }
        try {
            BluetoothClient.write(mBluetoothSocket, "test write");
            Log.v(TAG, "write complete");
        } catch (GroundControlBluetoothException e) {
            Utils.showAlert(this, e.getMessage());
        }
    }

    public void onClickRead(View unused) {
        if (mBluetoothSocket == null) {
            Utils.showAlert(this, "socket is null");
            return;
        }
        try {
            String s = BluetoothClient.read(mBluetoothSocket);
            Log.v(TAG, s);
        } catch (GroundControlBluetoothException e) {
            Utils.showAlert(this, e.getMessage());
        }

    }


}
