package com.ssimon.mybluetoochclientinteractive;

import java.util.Collection;
import java.util.Map;

/**
 * Utility class that provides static methods for validating variables. These 
 * methods throw an unchecked exception if the variable fails to meet the 
 * specified condition.
 * <p>
 * A common use of these methods is for validating method parameters. However,
 * these methods are intended to be used more generally, in any context in
 * which the user desires to validate a variable. Therefore, rather than 
 * throwing {@code IllegalArgumentException}, these methods throw exceptions 
 * appropriate for a wider range of contexts.
 * <p>
 * This class is stateless.
 *
 * @author ssimon
 */
final public class CheckerUtils {
    static final private String DEFAULT_NAME = "argument";
    static final private String EMPTY_FORMAT = "%s is empty";
    static final private String NULL_FORMAT = "%s is null";
    
    private CheckerUtils(){}

    static public void isExactly(int val, int targetVal, String argumentName) {
        inRange(val, targetVal, targetVal, argumentName);
    }
    
    /**
     * Throws an exception if the specified string is {@code null} or 0-length. The 
     * argumentName parameter is passed to the exception if one is thrown.
     *
     * @param str the string to check
     * @param argumentName a string describing the string to check
     *
     * @throws NullPointerException if the string to check is {@code null}
     * @throws IllegalStateException if the string to check is 0-length
     */
    static public void notNullOrEmpty(String str, final String argumentName) {
        notNull(str, argumentName);
        notEmpty(str, argumentName);
    }
    
    /**
     * Throws an exception if the specified string is {@code null}. The argumentName
     * parameter is passed to the exception if one is thrown.
     *
     * @param str the string to check
     * @param argumentName a string describing the string to check
     *
     * @throws NullPointerException if the string to check is {@code null}
     */
    static public void notNull(String str, String argumentName) {
        if (str == null) {
            String name = processArgumentName(argumentName);
            throw new NullPointerException(String.format(NULL_FORMAT, name));
        }
    }

    /**
     * Throws an exception if the specified string is 0-length. The 
     * argumentName parameter is passed to the exception if one is thrown.
     *
     * @param str the string to check
     * @param argumentName a string describing the string to check
     *
     * @throws IllegalStateException if the string to check is 0-length
     */
    static public void notEmpty(String str, String argumentName) {
        if (str.isEmpty()) {
            String name = processArgumentName(argumentName);
            throw new IllegalStateException(String.format(EMPTY_FORMAT, name));
        }
    }

    /**
     * Throws an exception if the specified collection is {@code null} or 0-length. The 
     * argumentName parameter is passed to the exception if one is thrown.
     *
     * @param col the collection to check
     * @param argumentName a string describing the collection to check
     *
     * @throws NullPointerException if the collection is {@code null}
     * @throws IllegalStateException if the collection is 0-length
     */
    static public void notNullOrEmpty(Collection<?> col, String argumentName) {
        notNull(col, argumentName);
        notEmpty(col, argumentName);
    }
    
    /**
     * Throws an exception if the specified collection is {@code null}. The 
     * argumentName parameter is passed to the exception if one is thrown.
     *
     * @param col the collection to check
     * @param argumentName a string describing the collection to check
     *
     * @throws NullPointerException if the collection is {@code null}
     */
    static public void notNull(Collection<?> col, String argumentName) {
        if (col == null) {
            String name = processArgumentName(argumentName);
            throw new NullPointerException(String.format(NULL_FORMAT, name));
        }
    }

    /**
     * Throws an exception if the specified collection is 0-length. The 
     * argumentName parameter is passed to the exception if one is thrown.
     *
     * @param col the collection to check
     * @param argumentName a string describing the collection to check
     *
     * @throws IllegalStateException if the collection is 0-length
     */
    static public void notEmpty(Collection<?> col, String argumentName) {
        if (col.isEmpty()) {
            String name = processArgumentName(argumentName);
            throw new IllegalStateException(String.format(EMPTY_FORMAT, name));
        }
    }
    
    /**
     * Throws an exception if the specified object is {@code null}. The argumentName
     * parameter is passed to the exception if one is thrown.
     *
     * @param obj the object to check
     * @param argumentName a string describing the object to check
     *
     * @throws NullPointerException if the object is {@code null}
     */
    static public void notNull(Object obj, String argumentName) {
        if (obj == null) {
            String name = processArgumentName(argumentName);
            throw new NullPointerException(String.format(NULL_FORMAT, name));
        }
    }
    
    /**
     * Throws an exception if the specified map is {@code null} or 0-length. The 
     * argumentName parameter is passed to the exception if one is thrown.
     *
     * @param map the map to check
     * @param argumentName a string describing the map to check
     *
     * @throws NullPointerException if the map is {@code null}
     * @throws IllegalStateException if the map is 0-length
     */
    static public void notNullOrEmpty(Map<?, ?> map, String argumentName) {
        notNull(map, argumentName);
        notEmpty(map, argumentName);
    }
    
    /**
     * Throws an exception if the specified map is {@code null}. The argumentName 
     * parameter is passed to the exception if one is thrown.
     *
     * @param map the map to check
     * @param argumentName a string describing the map to check
     *
     * @throws NullPointerException if the map is {@code null}
     */
    static public void notNull(Map<?, ?> map, String argumentName) {
        if (map == null) {
            String name = processArgumentName(argumentName);
            throw new NullPointerException(String.format(NULL_FORMAT, name));
        }
    }
    
    /**
     * Throws an exception if the specified map is 0-length. The 
     * argumentName parameter is passed to the exception if one is thrown.
     *
     * @param map the map to check
     * @param argumentName a string describing the map to check
     *
     * @throws IllegalStateException if the map is 0-length
     */
    static public void notEmpty(Map<?, ?> map, String argumentName) {
        if (map.isEmpty()) {
            String name = processArgumentName(argumentName);
            throw new IllegalStateException(String.format(EMPTY_FORMAT, name));
        }
    }
    
    /**
     * Throws an exception if the specified integer is less than the minimum 
     * value or equal to or greater than the upper bound. The argumentName 
     * parameter is passed to the exception if one is thrown.
     *
     * @param val the integer to check
     * @param argumentName a string describing the integer to check
     *
     * @throws IllegalStateException if {@code val < min | val > max}
     */
    static public void inRange(int val, int min, int max, String argumentName) {
        atLeast(val, min, argumentName);
        notGreaterThan(val, max, argumentName);
    }

    static public void inRange(float val, float min, float max, String argumentName) {
        atLeast(val, min, argumentName);
        notGreaterThan(val, max, argumentName);
    }

    static public void notGreaterThan(int val, int max, String argumentName) {
        if (val > max) {
            final String fmt = "%s (%d) is greater than max (%d)";
            String name = processArgumentName(argumentName);
            throw new IllegalStateException(String.format(fmt, name, val, max));
        }
    }

    static public void notGreaterThan(float val, float max, String argumentName) {
        if (val > max) {
            final String fmt = "%s (%f) is greater than max (%f)";
            String name = processArgumentName(argumentName);
            throw new IllegalStateException(String.format(fmt, name, val, max));
        }
    }

    /**
     * Throws an exception if the specified integer is less than the minimum 
     * value. The argumentName parameter is passed to the exception if one is 
     * thrown.
     *
     * @param val the integer to check
     * @param argumentName a string describing the integer to check
     *
     * @throws IndexOutOfBoundsException if {@code val < min}
     */
    static public void atLeast(int val, int min, String argumentName) {
        if (val < min) {
            final String fmt = "%s (%d) is less than min (%d)";
            String name = processArgumentName(argumentName);
            throw new IllegalStateException(String.format(fmt, name, val, min));
        }
    }

    static public void atLeast(long val, long min, String argumentName) {
        if (val < min) {
            final String fmt = "%s (%d) is less than min (%d)";
            throw new IndexOutOfBoundsException(String.format(fmt, argumentName, val, min));
        }
    }

    static public void atLeast(float val, float min, String argumentName) {
        if (val < min) {
            final String fmt = "%s (%f) is less than min (%f)";
            throw new IndexOutOfBoundsException(String.format(fmt, argumentName, val, min));
        }
    }

    /**
     * Throws an exception if the specified integer is greater than the upper 
     * bound. The argumentName parameter is passed to the exception if one is 
     * thrown.
     *
     * @param val the integer to check
     * @param argumentName a string describing the integer to check
     *
     * @throws IndexOutOfBoundsException if {@code val >= upperBound}
     */
    static public void lessThan(int val, int upperBound, String argumentName) {
        if (val >= upperBound) {
            final String fmt = "%s (%d) equals or exceeds upper bound (%d)";
            throw new IndexOutOfBoundsException(String.format(fmt, argumentName, val, upperBound));
        }
    }
    
    // Provides a default name for an argument if none is specified.
    static private String processArgumentName(String name) {
        return (name == null || name.trim().isEmpty()) ? DEFAULT_NAME : name;
    }
}